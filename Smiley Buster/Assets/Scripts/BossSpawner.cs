﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossSpawner : MonoBehaviour
{
    public GameObject comet;
    public float spawnTime;
    public float spawnDelay;
    void Start()
    {
        InvokeRepeating("SpawnComet", spawnTime, spawnDelay);
    }

    public void SpawnComet()
    {
        if (!BossHealth.dead)
        {
            Instantiate(comet, transform.position, Quaternion.identity);
        }
    }

    public void StopRepeating()
    {
        CancelInvoke();
    }
}
