﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float bulletSpeed = 10f;
    int hits = 0;
    Player player;
    MenuManager menuManager;

    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, 3f);
        player = FindObjectOfType<Player>();
        menuManager = FindObjectOfType<MenuManager>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.up * bulletSpeed * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("smiley"))
        {
            other.GetComponent<Smiley>().Explode();
            player.score += 150;
            menuManager.UpdateScore();
            
            hits++;
            if (hits >= 2)
            {
                player.score += 500;
                menuManager.UpdateScore();
            }
        } else if (other.CompareTag("evil"))
        {
            other.GetComponent<evil>().Explode();
            player.score += 300;
            menuManager.UpdateScore();
            hits++;
            if (hits >= 2)
            {
                player.score += 500;
                menuManager.UpdateScore();
            }
        } else if (other.CompareTag("boss"))
        {
            other.GetComponent<BossHealth>().TakeDamage(2.5f);
            Destroy(gameObject);
        }
    }
}
