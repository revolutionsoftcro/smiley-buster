﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SettingsFile
{
    public bool fullscreen;
    public int resolutionIndex;
    public float masterVolume;
    public float musicVolume;
    public string playerName;

    public SettingsFile(Settings settings)
    {
        fullscreen = settings.fullscreen;
        resolutionIndex = settings.resolutionIndex;
        masterVolume = settings.masterVolume;
        musicVolume = settings.musicVolume;
        playerName = settings.playerName;
    }
}
