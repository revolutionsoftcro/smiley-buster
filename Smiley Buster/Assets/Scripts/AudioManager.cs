﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public List<Clip> audioClips = new List<Clip>();
    AudioSource audioSource;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void PlayAudioClip(string clipName)
    {
        AudioClip clip = audioClips.FirstOrDefault(audioClip => audioClip.name == clipName).clip;
        audioSource.clip = clip;
        audioSource.Play();
    }
}

[System.Serializable]
public class Clip
{
    public AudioClip clip;
    public string name;
    public Clip(AudioClip audioClip, string clipName)
    {
        clip = audioClip;
        name = clipName;
    }
}