﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;

[System.Serializable]
public class ScoreData
{
    public int score;
    public string name;

    public ScoreData(int playerScore, string playerName)
    {
        score = playerScore;
        name = playerName;
    }
}
