﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class Settings : MonoBehaviour
{
    public AudioMixer mixer;
    public Player player;
    public Dropdown resolutionDropdown;
    public Toggle fullscreenToggle;
    public Slider masterVolumeSlider;
    public Slider musicVolumeSlider;
    public InputField playerNameInput;
    [HideInInspector]
    public bool fullscreen;
    [HideInInspector]
    public int resolutionIndex = 0;
    [HideInInspector]
    public float masterVolume = 0f;
    [HideInInspector]
    public float musicVolume = 0f;
    [HideInInspector]
    public string playerName = "Player";
    Resolution[] resolutions;
    

    public void Start()
    {
        resolutions = Screen.resolutions;
        resolutionDropdown.ClearOptions();

        int currentResolutionIndex = resolutionIndex;
        List<string> options = new List<string>();
        for (int i = 0; i < resolutions.Length; i++)
        {
            string option = resolutions[i].width + " x " + resolutions[i].height;
            options.Add(option);

            if (resolutions[i].width == Screen.currentResolution.width && resolutions[i].height == Screen.currentResolution.height)
            {
                currentResolutionIndex = i;
            }
        }

        resolutionDropdown.AddOptions(options);
        resolutionDropdown.value = currentResolutionIndex;
        resolutionDropdown.RefreshShownValue();
        resolutionIndex = currentResolutionIndex;

        InitializeSettings();
    }

    public void InitializeSettings()
    {
        SettingsFile settings = SaveSystem.LoadSettings();
        if (settings != null)
        {
            fullscreen = settings.fullscreen;
            resolutionIndex = settings.resolutionIndex;
            masterVolume = settings.masterVolume;
            musicVolume = settings.musicVolume;
            player.playername = settings.playerName;
            playerName = settings.playerName;
            SetFullscreen(fullscreen);
            SetSFXVolume(masterVolume);
            SetMusicVolume(musicVolume);
            SetPlayerName(playerName);
            fullscreenToggle.isOn = fullscreen;
            resolutionDropdown.value = resolutionIndex;
            masterVolumeSlider.value = masterVolume;
            musicVolumeSlider.value = musicVolume;
            playerNameInput.text = playerName;
        }
    }


    public void SetSFXVolume(float volume)
    {
        mixer.SetFloat("SFX", volume);
        masterVolume = volume;
        ApplySettings();
    }

    public void SetMusicVolume(float volume)
    {
        mixer.SetFloat("Music", volume);
        musicVolume = volume;
        ApplySettings();
    }

    public void SetPlayerName(string playername)
    {
        player.playername = playername;
        playerName = playername;
        ApplySettings();
    }

    public void SetResolution(int resIndex)
    {
        Resolution res = resolutions[resIndex];
        Screen.SetResolution(res.width, res.height, Screen.fullScreen);
        resolutionIndex = resIndex;
    }

    public void SetFullscreen(bool isFullscreen)
    {
        Screen.fullScreen = isFullscreen;
        fullscreen = isFullscreen;
        ApplySettings();
    }

    public void ApplySettings()
    {
        SaveSystem.SaveSettings(this);
    }
}
