﻿using System.IO;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighscoreTable : MonoBehaviour
{
    Transform entries;
    Transform template;
    List<Transform> highscoreEntryTransforms;
    List<ScoreData> highscoreEntries;
    public int maxHighScoreEntries = 6;

    private void Awake()
    {
        entries = GameObject.Find("LeaderboardEntries").transform;
        template = GameObject.Find("LeaderboardTemplate").transform;
        highscoreEntries = SaveSystem.LoadScores();

        template.gameObject.SetActive(false);

        if (highscoreEntries != null)
        {

            for (int i = 0; i < highscoreEntries.Count; i++)
            {
                for (int j = i + 1; j < highscoreEntries.Count; j++)
                {
                    if (highscoreEntries[j].score > highscoreEntries[i].score)
                    {
                        ScoreData tmp = highscoreEntries[i];
                        highscoreEntries[i] = highscoreEntries[j];
                        highscoreEntries[j] = tmp;
                    }
                }
            }

            if (highscoreEntries.Count > maxHighScoreEntries)
            {
                highscoreEntries.RemoveRange(maxHighScoreEntries, highscoreEntries.Count - maxHighScoreEntries);
            }

            highscoreEntryTransforms = new List<Transform>();
            foreach (ScoreData entry in highscoreEntries)
            {
                CreateHighscoreEntry(entry, entries, highscoreEntryTransforms);
            }
        }

    }

    void CreateHighscoreEntry(ScoreData entry, Transform container, List<Transform> transformList)
    {
        float templateHeight = 40f;
        Transform entryTransform = Instantiate(template, container);
        RectTransform entryRectTransform = entryTransform.GetComponent<RectTransform>();
        entryRectTransform.anchoredPosition = new Vector2(0, -templateHeight * transformList.Count);
        entryTransform.gameObject.SetActive(true);

        int rank = transformList.Count + 1;

        entryTransform.Find("Pos").GetComponent<Text>().text = rank.ToString();

        int score = entry.score;

        entryTransform.Find("Score").GetComponent<Text>().text = score.ToString();

        string name = entry.name.ToUpper();

        entryTransform.Find("Name").GetComponent<Text>().text = name;

        transformList.Add(entryTransform);
    }

    public void ClearEntries()
    {
        string path = Application.persistentDataPath + "/scores.bin";
        File.Delete(path);
        foreach (Transform transform in highscoreEntryTransforms)
        {
            transform.gameObject.SetActive(false);
        }
    }
}
