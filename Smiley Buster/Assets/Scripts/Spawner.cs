﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Spawner : MonoBehaviour
{
    public GameObject cometLevel1;
    public GameObject cometLevel2;
    public GameObject boss;
    public static GameObject smiley;
    public static int counter = 0;
    public float spawnTime = 0.2f;
    public float spawnDelay = 1f;
    public float level1Time = 60f;
    public float level2Time = 120f;
    public float level3Time = 60f;
    public Slider bossHealth;
    [HideInInspector]
    public int level = 1;
    MenuManager menuManager;
    float timer = 0f;
    bool pauseTimer = false;

    // Start is called before the first frame update
    void Start()
    {
        smiley = cometLevel1;
        menuManager = FindObjectOfType<MenuManager>();
        InvokeRepeating("SpawnComet", spawnTime, spawnDelay);
    }

    private void Update()
    {
        if (!pauseTimer)
        {
            timer += Time.deltaTime;
        }
        if (timer >= level1Time && counter <= 0 && level == 1)
        {
            menuManager.Level2();
            CancelInvoke();
            pauseTimer = true;
        } else if (timer >= level2Time && counter <= 0 && level == 2 || (timer >= level2Time+15 && level == 2))
        {
            menuManager.Level3();
            CancelInvoke();
            pauseTimer = true;
        } else if (timer >= level3Time && counter <= 0 && level == 3 || (timer >= level3Time+15 && level == 3))
        {
            SpawnBoss();
            CancelInvoke();
            level = 4;
            pauseTimer = true;
            
        }
    }

    public void StartLevel2()
    {
        timer = 0f;
        pauseTimer = false;
        InvokeRepeating("SpawnCometLevel2", spawnTime, spawnDelay);
    }
    public void StartLevel3()
    {
        timer = 0f;
        pauseTimer = false;
        InvokeRepeating("SpawnCometLevel3", spawnTime, spawnDelay);
    }

    public void SpawnComet()
    {
        if (timer <= level1Time)
        {
            GameObject temp = Instantiate(cometLevel1, new Vector3(Random.Range(-8f, 8f), transform.position.y, transform.position.z), Quaternion.identity);
            temp.GetComponent<Smiley>().SetGracePeriod(1.5f);
        }
    }

    public void SpawnCometLevel2()
    {
        level = 2;
        int random = Random.Range(0, 100);
        if (random <= 40 && timer <= level2Time)
        {
            GameObject smiley = Instantiate(cometLevel1, new Vector3(Random.Range(-8f, 8f), transform.position.y, transform.position.z), Quaternion.identity);
            smiley.GetComponent<Smiley>().speed = 2.5f;
            smiley.GetComponent<Smiley>().SetGracePeriod(1f);
        }
        else if (timer <= level2Time)
        {
            GameObject temp = Instantiate(cometLevel2, new Vector3(Random.Range(-8f, 8f), transform.position.y, transform.position.z), Quaternion.identity);
            temp.GetComponent<evil>().SetGracePeriod(0.5f);
        }
    }

    public void SpawnCometLevel3()
    {
        level = 3;
        if (timer <= level3Time)
        {
            Instantiate(cometLevel2, new Vector3(Random.Range(-8f, 8f), transform.position.y, transform.position.z), Quaternion.identity);
        }
    }

    public void SpawnBoss()
    {
        Instantiate(boss, new Vector3(0f, 7f, 0f), Quaternion.identity);
        bossHealth.gameObject.SetActive(true);
    }

    public void StopRepeating()
    {
        CancelInvoke();
    }
}
