﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossHealth : MonoBehaviour
{
    public float maxHealth = 100f;
    float health;
    Slider bossBar;
    MenuManager menuManager;
    Animator animator;
    public static bool dead = false;
    BossSpawner[] spawners;

    private void Start()
    {
        dead = false;
        spawners = FindObjectsOfType<BossSpawner>();
        bossBar = GameObject.Find("BossHealth").GetComponent<Slider>();
        menuManager = FindObjectOfType<MenuManager>();
        animator = GetComponent<Animator>();
        health = maxHealth;
    }

    public void TakeDamage(float damage)
    {
        health -= damage;
        bossBar.value = health;
        if (health <= 0)
        {
            if (!dead)
            {
                Die();
            }
        }
    }

    void Die()
    {
        GetComponent<PolygonCollider2D>().enabled = false;
        foreach (BossSpawner _bossSpawner in spawners)
        {
            _bossSpawner.StopRepeating();
        }
        foreach (evil _object in FindObjectsOfType<evil>())
        {
            _object.Explode();
            FindObjectOfType<Player>().score += 300;
        }
        foreach (Smiley _object in FindObjectsOfType<Smiley>())
        {
            _object.Explode();
            FindObjectOfType<Player>().score += 150;
        }
        FindObjectOfType<Player>().score += 2000;
        animator.SetBool("Explode", true);
        menuManager.BossKilled();
        dead = true;
    }

    public void DestroyMe()
    {
        Destroy(gameObject);
    }
}
