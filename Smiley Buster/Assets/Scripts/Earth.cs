﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Earth : MonoBehaviour
{
    public float maxHealth;
    public float health;
    public MenuManager menuManager;
    public Slider healthSlider;
    Player player;

    private void Start()
    {
        health = maxHealth;
        player = FindObjectOfType<Player>();
    }

    public void TakeDamage(float damage)
    {
        health -= damage;
        healthSlider.value = health;
        if (health <= 0)
        {
            menuManager.EndGame();
            player.gameObject.SetActive(false);
        }
    }
}
