﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;
using UnityEngine.SocialPlatforms.Impl;

public static class SaveSystem
{
    public static List<ScoreData> scores = new List<ScoreData>();
    public static void SaveScore(Player player)
    {
        scores = LoadScores();
        if ( scores == null)
        {
            scores = new List<ScoreData>();
        }
        scores.Add(new ScoreData(player.score, player.playername));
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/scores.bin";
        FileStream stream = new FileStream(path, FileMode.Create);

        formatter.Serialize(stream, scores);
        stream.Close();

    }

    public static List<ScoreData> LoadScores()
    {
        string path = Application.persistentDataPath + "/scores.bin";
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            List<ScoreData> scoreDatas = formatter.Deserialize(stream) as List<ScoreData>;
            stream.Close();

            return scoreDatas;
        } else
        {
            return null;
        }
    }

    public static void SaveSettings(Settings settings)
    {
        SettingsFile settingsFile = new SettingsFile(settings);
        string json = JsonUtility.ToJson(settingsFile);
        string subdir = Path.Combine(Application.persistentDataPath, "Config");
        DirectoryInfo dirInf = new DirectoryInfo(subdir);
        if (!dirInf.Exists)
        {
            dirInf.Create();
        }
        string path = Path.Combine(subdir, "settings.json");

        if (!File.Exists(path)) 
        {
            File.Create(path).Dispose();
        }

        File.WriteAllText(path, json);
    }

    public static SettingsFile LoadSettings()
    {
        string json;
        string subdir = Path.Combine(Application.persistentDataPath, "Config");
        DirectoryInfo dirInf = new DirectoryInfo(subdir);
        if (!dirInf.Exists)
        {
            dirInf.Create();
        }
        string path = Path.Combine(subdir, "settings.json");

        if (File.Exists(path))
        {
            json = File.ReadAllText(path);
            SettingsFile settingsFile = JsonUtility.FromJson<SettingsFile>(json);

            return settingsFile;
        } else
        {
            return null;
        }
    }
}
