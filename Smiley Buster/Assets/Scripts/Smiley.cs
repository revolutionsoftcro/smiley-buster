﻿using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using UnityEngine;
public class Smiley : MonoBehaviour
{
    public float speed = 1.5f;
    Animator animator;
    AudioManager audioManager;
    Earth earth;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        audioManager = FindObjectOfType<AudioManager>();
        earth = FindObjectOfType<Earth>();
        StartCoroutine(DestroyAfter(9f));
        Spawner.counter++;
    }

    // Update is called once per frame
    void Update()
    {
        if (animator.GetBool("Explode") == false)
        {
            transform.Translate(Vector3.down * Time.deltaTime * speed);
            if (transform.position.x <= -9f || transform.position.x >= 9f)
            {
                transform.SetPositionAndRotation(new Vector3(-transform.position.x / 1.05f, transform.position.y, transform.position.z), transform.rotation);
            }
        }
    }

    IEnumerator DestroyAfter(float time)
    {
        yield return new WaitForSeconds(time);
        if (animator.GetBool("Explode") == false)
        {
            earth.TakeDamage(5f);
            Explode();
        }
    }

    public void Explode()
    {
        GetComponent<CircleCollider2D>().enabled = false;
        if (animator != null)
        {
            animator.SetBool("Explode", true);
            audioManager.PlayAudioClip("Explosion");
        }
    }

    public void DestroyMe()
    {
        Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("evil"))
        {
            other.GetComponent<evil>().Explode();
        }

        if (other.CompareTag("smiley"))
        {
            other.GetComponent<Smiley>().Explode();
        }
    }

    public void SetGracePeriod(float time)
    {
        StartCoroutine(GracePeriod(time));
    }

    IEnumerator GracePeriod(float time)
    {
        GetComponent<CircleCollider2D>().enabled = false;
        yield return new WaitForSeconds(time);
        GetComponent<CircleCollider2D>().enabled = true;
    }

    private void OnDestroy()
    {
        Spawner.counter--;
    }
}
