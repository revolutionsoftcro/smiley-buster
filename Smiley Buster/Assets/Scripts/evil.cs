﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class evil : MonoBehaviour
{
    public float speed = 3f;
    Animator animator;
    AudioManager audioManager;
    Earth earth;
    bool exploded = false;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        audioManager = FindObjectOfType<AudioManager>();
        earth = FindObjectOfType<Earth>();
        StartCoroutine(DestroyAfter(5.3f));
        Spawner.counter++;
    }

    // Update is called once per frame
    void Update()
    {
        if (animator.GetBool("Explode") == false)
        {
            transform.Translate(Vector3.down * Time.deltaTime * speed);
        }
    }

    IEnumerator DestroyAfter(float time)
    {
        yield return new WaitForSeconds(time);
        if (animator.GetBool("Explode") == false)
        {
            earth.TakeDamage(20f);
            audioManager.PlayAudioClip("Explosion");
            Destroy(gameObject);
        }
    }

    public void Explode()
    {
        if (!exploded)
        {
            GetComponent<CircleCollider2D>().enabled = false;
            GameObject smiley1 = Instantiate(Spawner.smiley, new Vector3(transform.position.x - 0.5f, transform.position.y, transform.position.z), Quaternion.Euler(0f, 0f, -45f));
            GameObject smiley2 = Instantiate(Spawner.smiley, new Vector3(transform.position.x + 0.5f, transform.position.y, transform.position.z), Quaternion.Euler(0f, 0f, 45f));
            smiley1.GetComponent<Smiley>().SetGracePeriod(0.2f);
            smiley2.GetComponent<Smiley>().SetGracePeriod(0.2f);
            animator.SetBool("Explode", true);
            audioManager.PlayAudioClip("Explosion");
            exploded = true;
        }
        
    }

    public void DestroyMe()
    {
        Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("evil"))
        {
            other.GetComponent<evil>().Explode();
        }

        if (other.CompareTag("smiley"))
        {
            other.GetComponent<Smiley>().Explode();
        }
    }

    public void SetGracePeriod(float time)
    {
        StartCoroutine(GracePeriod(time));
    }

    IEnumerator GracePeriod(float time)
    {
        GetComponent<CircleCollider2D>().enabled = false;
        yield return new WaitForSeconds(time);
        GetComponent<CircleCollider2D>().enabled = true;
    }

    private void OnDestroy()
    {
        Spawner.counter--;
    }
}
