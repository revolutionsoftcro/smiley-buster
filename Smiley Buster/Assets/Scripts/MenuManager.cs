﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    public RawImage backgroundImage;
    public Texture level2Background;
    public Texture level3Background;
    public Player player;
    public Spawner spawner;
    public GameObject menu;
    public GameObject level2Panel;
    public GameObject level3Panel;
    public GameObject GUI;
    public GameObject gameOverPanel;
    public GameObject gameWinPanel;
    public Text scoreText;
    public Text endScoreText;
    public Text gameWinScoreText;
    int score;
    bool scoreSaved = false;

    public void UpdateScore()
    {
        scoreText.text = "Score: " + player.score;
    }

    public void StartGame()
    {
        player.enabled = true;
        spawner.enabled = true;
        menu.gameObject.SetActive(false);
        GUI.SetActive(true);
    }

    public void Level2()
    {
        player.enabled = false;
        spawner.enabled = false;
        GUI.SetActive(false);
        level2Panel.SetActive(true);
    }

    public void Level3()
    {
        player.enabled = false;
        spawner.enabled = false;
        GUI.SetActive(false);
        level3Panel.SetActive(true);
    }

    public void StartLevel2()
    {
        backgroundImage.texture = level2Background;
        level2Panel.SetActive(false);
        GUI.SetActive(true);
        player.enabled = true;
        spawner.enabled = true;
        spawner.level = 2;
        spawner.StartLevel2();
    }
    public void StartLevel3()
    {
        backgroundImage.texture = level3Background;
        level3Panel.SetActive(false);
        GUI.SetActive(true);
        player.enabled = true;
        spawner.enabled = true;
        spawner.level = 3;
        spawner.StartLevel3();
    }

    public void BossKilled()
    {
        score = player.score;
        if (!scoreSaved)
        {
            SaveSystem.SaveScore(player);
            scoreSaved = true;
            Debug.Log("Boss killed, Score Saved.");
        }
        player.enabled = false;
        spawner.enabled = false;
        GUI.SetActive(false);
        gameWinPanel.SetActive(true);
        gameWinScoreText.text = "Your Score: " + score;
    }

    public void EndGame()
    {
        level2Panel.SetActive(false);
        level3Panel.SetActive(false);
        score = player.score;
        if (!scoreSaved)
        {
            SaveSystem.SaveScore(player);
            scoreSaved = true;
            Debug.Log("You died, Score Saved.");
        }
        spawner.StopRepeating();
        GUI.SetActive(false);
        endScoreText.text = "Your Score: " + score;
        gameOverPanel.SetActive(true);
    }

    public void ExitToMainMenu()
    {
        Pause.isPaused = false;
        Time.timeScale = 1f;
        SceneManager.LoadSceneAsync(0);
    }
    
    public void ExitGame()
    {
        Application.Quit();
    }
}
